#include <stdio.h>

long fib(int n) {
	if (n < 2) {
		return n;
	}
	return fib(n - 1) + fib(n - 2);
}


int main(void) {
	long int res = fib(45);
	printf("%ld\n", res);
}