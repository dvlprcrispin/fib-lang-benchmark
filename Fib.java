package com.epic.fib;
//thx lex
public class Fib {

    public static void main(String[] args) {
        System.out.println(fib(45));
    }

    public static int fib(int i) {
        if(i < 2) {
            return i;
        }
        return fib(i - 1) + fib(i - 2);
    }
}